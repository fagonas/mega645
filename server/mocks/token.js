/*jshint node:true*/
module.exports = function(app) {
  var express = require('express');
  var tokenRouter = express.Router();

  //tokenRouter.get('/', function(req, res) {
  //  res.send({
  //    'token': []
  //  });
  //});

  var user1 = {
    email: "test@owlsoft.net",
    password: "123"
  };
  tokenRouter.post('/', function(req, res) {
    if(req.body.email === user1.email && req.body.password === user1.password){
      console.log('post data: ', req);
      res.send({
        token: "123mm123"
      });
    }else {
      res.status(401).end();
    }

  });

  //tokenRouter.get('/:id', function(req, res) {
  //  res.send({
  //    'token': {
  //      id: req.params.id
  //    }
  //  });
  //});
  //
  //tokenRouter.put('/:id', function(req, res) {
  //  res.send({
  //    'token': {
  //      id: req.params.id
  //    }
  //  });
  //});

  //tokenRouter.delete('/:id', function(req, res) {
  //  res.status(204).end();
  //});

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/token', require('body-parser').json());
  app.use('/api/auth', tokenRouter);
};
