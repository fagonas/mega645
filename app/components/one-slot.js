import Ember from 'ember';

function createOdometerElement(id, number){
  console.log("create new odo ele!");
  var elOdo = Ember.$("#"+id);

  var od1 = new Odometer({
    el: elOdo[0],
    value: number,

    // Any option (other than auto and selector) can be passed in here
    format: '(,ddd).ddd',
    //theme: 'car',
    theme: 'digital'
  });
  return od1;
}

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didRender(){
    this._super(...arguments);
    console.log("did render ele");
    createOdometerElement(this.odoId, this.number);
  }

});
