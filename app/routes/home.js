import Ember from 'ember';


function getRandomInt() {
  var min = 1;
  var max = 45;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function padding(num) {
  var size = 2;
  var s = num + "";
  while (s.length < size) {
    s = "0" + s;
  }
  return s;
}

function reset(){
  odo1.innerHTML = 10;
  odo2.innerHTML = 10;
  odo3.innerHTML = 10;
  odo4.innerHTML = 10;
  odo5.innerHTML = 10;
  odo6.innerHTML = 10;
}


export default Ember.Route.extend({
  actions: {
    generate(){
      console.log("action generate clicked!");
      reset();
      var numbers = [];
      for (var i = 0; i < 6; i++) {
        while (1) {
          var tempNumber = getRandomInt();
          if (!numbers.includes(tempNumber)) {
            numbers.push(tempNumber);
            break;
          }
        }
      }
      console.log("generated numbers: ", numbers);
      odo1.innerHTML = numbers[0];
      odo2.innerHTML = numbers[1];
      odo3.innerHTML = numbers[2];
      odo4.innerHTML = numbers[3];
      odo5.innerHTML = numbers[4];
      odo6.innerHTML = numbers[5];
    }
  }
});
